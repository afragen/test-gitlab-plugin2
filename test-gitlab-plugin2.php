<?php
/*
Plugin Name:       Test GitLab Plugin2
Plugin URI:        https://gitlab.com/afragen/test-gitlab-plugin2/
Description:       This plugin is used for testing functionality of GitLab updating of plugins.
Version:           0.4.5
Author:            Andy Fragen
License:           GNU General Public License v2
License URI:       http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
GitLab Plugin URI: https://gitlab.com/afragen/test-gitlab-plugin2/
GitLab Branch:     master
Requires WP:       4.2
Requires PHP:      5.4
*/